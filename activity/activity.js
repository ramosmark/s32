import http from 'http'

const port = 8000

const courses = [
    {
        "courseName": "HTML 101",
        "isAvailable": true
    },
    {
        "courseName": "History of Ice Cream",
        "isAvailable": false
    }
]

http.createServer((req, res) => {
    if (req.url === '/' &&  req.method === 'GET') {
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.end('Welcome to Booking System')
    } else if (req.url === '/profile' &&  req.method === 'GET') {
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.end('Welcome to your profile!')
    } else if (req.url === '/courses' &&  req.method === 'GET') {
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.write(JSON.stringify(courses))
		res.end()
    } else if (req.url === '/addcourse' &&  req.method === 'POST') {
        let requestBody = ''

        req.on('data', function(data){
			requestBody = data.toString()
		})

        req.on('end', function() {
			requestBody = JSON.parse(requestBody)
			
			const newCourse = {
				"courseName": requestBody.courseName,
				"isAvailable": requestBody.isAvailable
			}
			courses.push(newCourse)

			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(newCourse))
			res.end()
		})
    }
}).listen(port)

console.log(`Server is running at localhost: ${port}`)